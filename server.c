#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/sctp.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
//#include <stdint.h>

#define RECVBUFSIZE             4096
#define PPID                    1234
#define ADDRESS                 "127.0.0.1"
#define PORT                    8887
#define MESSAGE                 "Hello, Client\0"

int main() {
    int SctpSocket, n, flags;
    socklen_t from_len;

    struct sockaddr_in addr = {0};
    struct sctp_sndrcvinfo sinfo = {0};
    struct sctp_sndrcvinfo sinfo2 = {0};
    struct sctp_event_subscribe event = {0};
    char pRecvBuffer[RECVBUFSIZE + 1] = {0};

    char * szAddress;
    int iPort;
    char szMsg[] = MESSAGE;
    int iMsgSize;

    //get the arguments
    szAddress = ADDRESS;
    iPort = PORT;
    iMsgSize = strlen(szMsg);
    if (iMsgSize > 1024) {
        perror("Message is too big\n");
        return 0;
    }

    //here we may fail if sctp is not supported
    SctpSocket = socket(AF_INET, SOCK_SEQPACKET, IPPROTO_SCTP);
    printf("socket created\n");

    //make sure we receive MSG_NOTIFICATION
    setsockopt(SctpSocket, IPPROTO_SCTP, SCTP_EVENTS, &event, sizeof (struct sctp_event_subscribe));
    printf("setsockopt succeeded\n");

    addr.sin_family = AF_INET;
    addr.sin_port = htons(iPort);
    addr.sin_addr.s_addr = htonl(INADDR_ANY);

    //bind to specific server address and port
    bind(SctpSocket, (struct sockaddr *) &addr, sizeof (struct sockaddr_in));
    printf("bind succeeded\n");

    //wait for connections
    listen(SctpSocket, 3);
    printf("listen succeeded\n");

    int count = 0, i;
    char send_data[] = MESSAGE;

    while (1) {
        //each time erase the stuff
        flags = 0;
        memset((void *) &addr, 0, sizeof (struct sockaddr_in));
        from_len = (socklen_t)sizeof (struct sockaddr_in);
        memset((void *) &sinfo, 0, sizeof (struct sctp_sndrcvinfo));
        memset((void *) &sinfo2, 0, sizeof (struct sctp_sndrcvinfo));

        n = sctp_recvmsg(SctpSocket, (void*) pRecvBuffer, RECVBUFSIZE, (struct sockaddr *) &addr, &from_len, &sinfo, &flags);
        if (-1 == n) {
            printf("Error with sctp_recvmsg: -1... waiting\n");
            printf("errno: %d\n", errno);
            perror("Description: ");
            sleep(1);
            continue;
        }

        if (flags & MSG_NOTIFICATION) {
            printf("Notification received!\n");
            printf("From %s:%u\n", inet_ntoa(addr.sin_addr), ntohs(addr.sin_port));
        } else {
            printf("Received from %s:%u on stream %d, PPID %d.: %s\n",
                    inet_ntoa(addr.sin_addr),
                    ntohs(addr.sin_port),
                    sinfo.sinfo_stream,
                    ntohl(sinfo.sinfo_ppid),
                    pRecvBuffer
                    );
        }

        n = sctp_recvmsg(SctpSocket, (void*) pRecvBuffer, RECVBUFSIZE, (struct sockaddr *) &addr, &from_len, &sinfo2, &flags);
        if (-1 == n) {
            printf("Error with sctp_recvmsg: -1... waiting\n");
            printf("errno: %d\n", errno);
            perror("Description: ");
            sleep(1);
            continue;
        }

        if (flags & MSG_NOTIFICATION) {
            printf("Notification received!\n");
            printf("From %s:%u\n", inet_ntoa(addr.sin_addr), ntohs(addr.sin_port));
        } else {
            printf("Received from %s:%u on stream %d, PPID %d.: %s\n",
                    inet_ntoa(addr.sin_addr),
                    ntohs(addr.sin_port),
                    sinfo2.sinfo_stream,
                    ntohl(sinfo.sinfo_ppid),
                    pRecvBuffer
                    );
        }
        //send message to client
        printf("Sending to client: %s\n", send_data);
        sctp_sendmsg(SctpSocket, (const void *) send_data, strlen(send_data), (struct sockaddr *) &addr, from_len, htonl(PPID), 0, 0 /*stream 0*/, 0, 0);

        //close server when exit is received
        if (0 == strcmp(pRecvBuffer, "exit")) {
            break;
        }
    }//while

    printf("exiting...\n");

    close(SctpSocket);
    return (0);
}

