#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netinet/sctp.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>

#define RECVBUFSIZE     4096
#define PPID            1234
#define ADDRESS         "127.0.0.1"
#define PORT            8887
#define MESSAGE         "Hello,server\0"
#define REQ_MSG         "Send me something\0"

int main(int argc, char* argv[]) {
    int SctpSocket, in, flags;
    socklen_t opt_len;
    char * szAddress;
    int Port;
    char * szMsg;
    int MsgSize, ReqSize;
    char a[] = MESSAGE;
    char b[] = REQ_MSG;
    MsgSize = sizeof(a);
    ReqSize = sizeof(b);

    struct sockaddr_in servaddr = {0};
    struct sctp_status status = {0};
    struct sctp_sndrcvinfo sndrcvinfo = {0};
    struct sctp_event_subscribe events = {0};
    struct sctp_initmsg initmsg = {0};
    char * szRecvBuffer[RECVBUFSIZE + 1] = {0};
    socklen_t from_len = (socklen_t) sizeof (struct sockaddr_in);

    szAddress = ADDRESS;
    Port = PORT;

    printf("Starting SCTP client connection to %s:%u\n", szAddress, Port);


    SctpSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_SCTP);
    printf("socket created...\n");

    initmsg.sinit_num_ostreams = 2;
    initmsg.sinit_max_instreams = 2;
    setsockopt(SctpSocket, IPPROTO_SCTP, SCTP_INITMSG, &initmsg, sizeof (initmsg));
    printf("setsockopt succeeded...\n");

    bzero((void *) &servaddr, sizeof (servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_port = htons(Port);
    servaddr.sin_addr.s_addr = inet_addr(szAddress);


    connect(SctpSocket, (struct sockaddr *) &servaddr, sizeof (servaddr));
    printf("connect succeeded...\n");

    opt_len = (socklen_t) sizeof (struct sctp_status);
    getsockopt(SctpSocket, IPPROTO_SCTP, SCTP_STATUS, &status, &opt_len);

    while (1) {
        sctp_sendmsg(SctpSocket, a, MsgSize, NULL, 0, 0, 0, 0, 0, 0);
        printf("Message sended\n");
        sctp_sendmsg(SctpSocket, b, ReqSize, NULL, 0, 0, 0, 1, 0, 0);
        printf("Request sended. Waiting for response\n");
        //read response from test server
        in = sctp_recvmsg(SctpSocket, (void*) szRecvBuffer, RECVBUFSIZE, (struct sockaddr *) &servaddr, &from_len, &sndrcvinfo, &flags);
        if (in > 0 && in < RECVBUFSIZE - 1) {
            szRecvBuffer[in] = 0;
            printf("Received from server: %s\n", szRecvBuffer);
            sleep(1);
        }
    }
    printf("exiting...\n");

    close(SctpSocket);
    return 0;
}

